module register_tb ();

parameter BITS = 4; // must be multiple of 4 to allow 'auto-config'
parameter CLKP = 10;
parameter STEP = BITS/4;

reg clk, rst, enb;
reg[BITS-1:0] idata;
wire[BITS-1:0] odata;

task reg_data;
	input[BITS-1:0] data;
	begin
		$display("[%04g] Register data {%h}", $time,data);
		idata = data;
		#(1*CLKP); enb = 1'b1;
		#(1*CLKP); enb = 1'b0;
		$write("[%04g] Checking data {%h} => ", $time,odata);
		if (odata==data) $display("[OK]");
		else $display("[ERROR!]");
	end
endtask

// reset stuffs
initial begin
	clk = 1'b0; rst = 1'b0;
	enb = 1'b0; idata = {STEP{4'h0}};
	#(1*CLKP) rst = 1'b0;
	#(5*CLKP) rst = 1'b1;
	#(5*CLKP) rst = 1'b0;
end

// generate clock
always #(CLKP/2) clk = !clk;

//generate stimuli
always begin
	$display("[%04g] Testing register module...", $time);
	reg_data({STEP{4'ha}});
	reg_data({STEP{4'h5}});
	$finish;
end

register #(BITS) dut (clk, rst, enb, idata, odata);

endmodule
