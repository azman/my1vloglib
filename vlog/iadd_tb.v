module iadd_tb ();

parameter BITS = 8;

reg[BITS-1:0] iA, iB, iC;
wire[BITS-1:0] oS, oC, oP;

integer numC, ecnt;
reg[BITS-1:0] repA, repB;
reg[BITS-1:0] test, ends;
reg[BITS-1:0] tS;
reg tC;
reg[BITS:0] chkS;

initial begin
	$write("-- Start test for %g-bit integer adder... ",BITS);
	ecnt = 0;
	ends = 2**BITS;
	for (numC=0;numC<2;numC=numC+1) begin
		iC[0] = numC;
		for (repA=0;repA<ends;repA=repA+1) begin
			iA = repA;
			for (repB=0;repB<ends;repB=repB+1) begin
				iB = repB;
				#5;
				test = repA + repB + numC;
				{ tC,tS } = test;
				chkS = {oC[BITS-1],oS};
				if ((tC!==oC[BITS-1])||(tS!==oS)) begin
					ecnt = ecnt + 1;
					$write("\n** [ERROR] {%g+%g+%b} ",iA,iB,iC[0]);
					$write("= {%g:%b,%g} ",test,tC,tS);
					$write(" got {%g:%b,%g} (%d)\n",chkS,oC[BITS-1],oS,ecnt);
				end
				#5;
			end
		end
		$write(".");
	end
	$display(" done.");
	if (ecnt==0) begin
		$display("-- Module iadd (%g-bits) verified.",BITS);
	end
	else begin
		$display("** Module iadd with error(s) {ErrorCount=%g}",ecnt);
	end
	$finish;
end

//defparam dut.BITS = BITS; // verilog2001 accepts # block
iadd #(BITS) dut (iA,iB,iC,oS,oC,oP);

endmodule
