module zbuffer (ienb, idat, odat);
parameter BITS = 8;
input ienb;
input[BITS-1:0] idat;
output[BITS-1:0] odat;
wire[BITS-1:0] odat;
assign odat = (ienb==1)? idat : {BITS{1'bz}};
endmodule
