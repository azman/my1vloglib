module decoder_tb ();

parameter SEL_SIZE = 3;
parameter OUT_SIZE = 2**SEL_SIZE;

reg[SEL_SIZE-1:0] iS;
wire[OUT_SIZE-1:0] oY, oX;
reg[OUT_SIZE-1:0] oZ; // for checking

integer loop;
initial
begin
	// test 1-hot
	oZ = 1; // only LSB 1, the rest should be zero
	$display("[%3g] 1-HOT Encoding... %b",$time,oZ);
	$display("[%3g] Start test for %g-%g decoder...",$time, SEL_SIZE, OUT_SIZE);
	for (loop=0;loop<OUT_SIZE;loop=loop+1) begin
		iS = loop;
		#10;
		$write("[%3g] Input=%b, Output=%b => ",$time, iS, oY);
		if (oY != oZ) $display("[ERROR] Expected Output=%b",oZ);
		else $display("[OK]");
		oZ = oZ << 1;
	end
	$display("[%3g] End test.",$time);
	// test 1-cold
	oZ = 1; // only LSB 1, the rest should be zero
	$display("[%3g] 1-COLD Encoding... %b",$time,~oZ);
	$display("[%3g] Start test for %g-%g decoder...",$time, SEL_SIZE, OUT_SIZE);
	for (loop=0;loop<OUT_SIZE;loop=loop+1) begin
		iS = loop;
		#10;
		$write("[%3g] Input=%b, Output=%b => ",$time, iS, oX);
		if (oX != ~oZ) $display("[ERROR] Expected Output=%b",~oZ);
		else $display("[OK]");
		oZ = oZ << 1;
	end
	$display("[%3g] End test.",$time);
	$finish;
end

decoder #(.SEL_SIZE(SEL_SIZE)) dut (iS, oY);
decoder #(.SEL_SIZE(SEL_SIZE),.ONE_COLD(1))  d2t (iS, oX);

endmodule
