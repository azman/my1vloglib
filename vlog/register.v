module register (clk, rst, enb, idata, odata);

parameter BITS = 8;

input clk, rst, enb;
input[BITS-1:0] idata;
output[BITS-1:0] odata;
reg[BITS-1:0] odata;
reg[BITS-1:0] tdata;

always @(posedge clk or rst) begin
	tdata = odata;
	if (rst===1'b1) begin // asynchronous reset!?
		odata <= {BITS{1'b0}};
	end
	else begin
		if (enb==1'b1) odata <= idata;
		else odata <= tdata;
	end
end

endmodule
