module zbuffer_tb ();
parameter BITS = 4;
parameter CLKP = 10;
reg denb;
reg[BITS-1:0] ddat;
wire[BITS-1:0] mdat;

task chk_state;
	begin
		#(CLKP); // let everything settle down
		$write("[%04g] Idata={%b}, Odata={%b}, denb={%b} => ",
			$time,ddat,mdat,denb);
		if (denb==1) begin
			if (mdat!==ddat) $display("[ERROR]");
			else $display("[OK!]");
		end
		else begin
			if (mdat!=={BITS{1'bz}}) $display("[ERROR]");
			else $display("[OK!]");
		end
	end
endtask

// reset stuffs
initial begin
	denb = 1'b0; ddat = 1'b0;
end

//generate stimuli
always begin
	$display("[%04g] Testing tri-state buffer module...", $time);
	ddat = {BITS{1'b1}};
	chk_state;
	denb = 1'b1;
	chk_state;
	ddat = {BITS{1'b0}};
	chk_state;
	denb = 1'b0;
	chk_state;
	$finish;
end

zbuffer #(BITS) dut (denb, ddat, mdat);

endmodule
