# makefile for using my1vlog tool

# allow a 'local' makefile
LOCALMAKE ?= makefile.local
ifneq ($(wildcard $(LOCALMAKE)),)
include $(LOCALMAKE)
endif

# command line can override local makefile
FIND_ARG = $(foreach a,$1,$(if $(value $a),$a='"$($a)"'))
FIND_IARG = $(foreach a,$1,$(if $(value $a),$a=$($a)))

TOOL_ARG := $(call FIND_IARG,icarus)
ifneq ($(TOOL_ARG),)
	DOTEST = $(word 2,$(subst =, ,$(TOOL_ARG)))
	ifeq ($(DOTEST),1)
		GOTOOL = --icarus
	endif
endif

SIMCHK = --test
COMP_ARG := $(call FIND_IARG,compile)
ifneq ($(COMP_ARG),)
	DOCOMP = $(word 2,$(subst =, ,$(COMP_ARG)))
	ifeq ($(DOCOMP),1)
		SIMCHK =
	endif
endif

DOFROM ?= vlib
DOPATH ?= vlog

DOLIST = $(DOFROM).list
DOFIND = $(DOPATH)/$(DOFROM).list
DOPREP = .

ifneq ($(wildcard $(DOFIND)),)
DOLIST = $(DOFIND)
DOPREP = $(DOPATH)
endif

TBLIST = $(subst _tb.v,,$(sort $(wildcard *_tb.v)))
TBXTRA = $(subst $(DOPATH)/,,$(sort $(wildcard $(DOPATH)/*.v)))
TBLIST += $(subst .v,,$(TBXTRA))

.PHONY: help prep list wipe clean check

help:
	@echo "Run 'make <mod>'"
	@echo "  <mod> = { $(TBLIST) }"
	@echo "Note: icarus=1 in make command will force the use of Icarus Verilog"
	@echo "Path: $(DOPATH)"

%: $(DOPATH)/%.v $(DOPATH)/%_tb.v
	@my1vlog $(GOTOOL) --path $(DOPATH) $(SIMCHK) $@ 2>/dev/null

%: $(DOPATH)/%_tb.v
	@my1vlog $(GOTOOL) --path $(DOPATH) $@ --raw 2>/dev/null

%: $(DOPATH)/%.v
	@my1vlog $(GOTOOL) --path $(DOPATH) $@ 2>/dev/null

%: %.v %_tb.v
	@my1vlog $(GOTOOL) --path . --test $@ 2>/dev/null

%: %_tb.v
	@my1vlog $(GOTOOL) --path . $@ --raw 2>/dev/null

%: %.v
	@my1vlog $(GOTOOL) --path . $@ 2>/dev/null

prep: $(DOLIST)
	@my1vlog $(GOTOOL) --from $(DOLIST) --path $(DOPREP) 2>/dev/null

list:
	@my1vlog $(GOTOOL) --path . --list $(DOLIST) 2>/dev/null

wipe:
	@my1vlog $(GOTOOL) --wipe 2>/dev/null

clean: wipe

check:
	@my1vlog $(GOTOOL) --path . --check 2>/dev/null
